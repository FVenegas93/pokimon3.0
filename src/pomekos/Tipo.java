package pomekos;

public enum Tipo {
PLANTA("Planta"), VENENO("Veneno"), AGUA("Agua"), PSIQUICO("Ps�quico"), NORMAL("Normal");
	
	protected String nombreTipo;
	
	private Tipo(String nombreTipo) {
		this.nombreTipo=nombreTipo;
	}
	
	public String getNombreTipo() {
		return this.nombreTipo;
	}
}
