package pomekos;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.LinkedList;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;


public class SimuladorCombates extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	
	Movimientos placaje = new Movimientos("PLACAJE", 40, Tipo.NORMAL, 35);
	Movimientos bombalodo = new Movimientos("BOMBA LODO", 90, Tipo.VENENO, 10);
	Movimientos hojaafilada = new Movimientos ("HOJA AFILADA", 55, Tipo.PLANTA, 20);
	Movimientos megaagotar = new Movimientos ("MEGA AGOTAR", 40, Tipo.PLANTA, 10);
	Movimientos psiquico = new Movimientos ("PS�QUICO", 90, Tipo.PSIQUICO, 10);
	Movimientos rayoburbuja = new Movimientos ("RAYO BURBUJA", 65, Tipo.AGUA, 20);
	Movimientos golpecuerpo = new Movimientos ("GOLPE CUERPO", 85, Tipo.NORMAL, 15);
	
	Pokemon bulbasaur = new Pokemon("BULBASAUR", 100, 294, 197, 197, 229, 229, 189, Tipo.PLANTA, Tipo.VENENO, 
			placaje, bombalodo, hojaafilada, megaagotar);
	
	Pokemon poliwhirl = new Pokemon("POLIWHIRL", 100, 334, 229, 229, 199, 199, 279, Tipo.AGUA, null,
			placaje, rayoburbuja, psiquico, golpecuerpo);
	
	LinkedList<Pokemon> listaPokemon = new LinkedList<Pokemon>();
		
	protected int hp_aux_aliado;
	protected int hp_aux_rival;
	protected String aux="";
	protected String cad2;
	private double stab;
	private double efectividad;
	private int dmgg;
	private int random_Move;
	
	protected int cont;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SimuladorCombates frame = new SimuladorCombates();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public SimuladorCombates() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, 900, 800);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		setContentPane(contentPane);
				
		listaPokemon.add(bulbasaur);
		listaPokemon.add(poliwhirl);
		
		JLabel lb1 = new JLabel("Tu Pok�mon");
		lb1.setBounds(30, 30, 100, 30);
		contentPane.add(lb1);
		
		JLabel lb2 = new JLabel("Pok�mon Rival");
		lb2.setBounds(160, 30, 100, 30);
		contentPane.add(lb2);
		
		JComboBox<Pokemon> cb1 = new JComboBox<Pokemon>();
		for (Pokemon p : listaPokemon) {
			cb1.addItem(p);
		}		
		cb1.setBounds(30, 80, 100, 30);
		contentPane.add(cb1);
		
		JComboBox<Pokemon> cb2 = new JComboBox<Pokemon>();
		for (Pokemon p : listaPokemon) {
			cb2.addItem(p);
		}
		cb2.setBounds(160, 80, 100, 30);
		contentPane.add(cb2);
		
		JButton aceptar = new JButton("ACEPTAR");
		aceptar.setBounds(30, 130, 100, 30);
		contentPane.add(aceptar);
		
		aceptar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				Pokemon pkmn_aliado = (Pokemon) cb1.getSelectedItem();
				Pokemon pkmn_rival = (Pokemon) cb2.getSelectedItem();
				
				JLabel nom_Aliado = new JLabel(pkmn_aliado.toString());
				nom_Aliado.setBounds(30, 370, 100, 30);
				contentPane.add(nom_Aliado);
				
				JLabel nom_Rival = new JLabel(pkmn_rival.toString());
				nom_Rival.setBounds(400, 170, 100, 30);
				contentPane.add(nom_Rival);
								
				switch(pkmn_aliado.toString()) {
				case "BULBASAUR": 
					insertarPokemonAliado(bulbasaur, "src\\back_sprite_bulbasaur.png");
					combatir(pkmn_aliado, pkmn_rival);
					
				break;
				case "POLIWHIRL": 
					insertarPokemonAliado(poliwhirl, "src\\back_sprite_poliwhirl.png");
					combatir(pkmn_aliado, pkmn_rival);
					
				break;				
				}
				
				switch(pkmn_rival.toString()) {
				case "BULBASAUR":
					insertarPokemonRival(bulbasaur, "src\\front_sprite_bulbasaur.png");
				break;
				case "POLIWHIRL":
					insertarPokemonRival(poliwhirl, "src\\front_sprite_poliwhirl.png");
				break;	
				}	
			}			
		});
	}
	
	public void insertarPokemonAliado(Pokemon p, String ruta) {
		JLabel lb = new JLabel(new ImageIcon(ruta));
		lb.setBounds(30, 400, 100, 100);
		contentPane.add(lb);
						
		String maximum_hp = String.valueOf(p.getHp());
		JLabel max_hp = new JLabel("/"+maximum_hp);
		max_hp.setBounds(110, 525, 30, 20);
		contentPane.add(max_hp);	
	}
	
	public void insertarPokemonRival(Pokemon p, String ruta) {
		JLabel lb = new JLabel(new ImageIcon(ruta));
		lb.setBounds(400, 200, 100, 100);
		contentPane.add(lb);
								
		String maximum_hp = String.valueOf(p.getHp());
		JLabel max_hp = new JLabel("/"+maximum_hp);
		max_hp.setBounds(480, 325, 30, 20);
		contentPane.add(max_hp);
	}
	
	public void cambiarBG_Mov(Movimientos m, JButton btn) {
		
		switch(m.getTipoMov().getNombreTipo()) {
		case "Planta":
			btn.setBackground(new Color(0, 255, 0));
		break;
		case "Agua":
			btn.setBackground(new Color(59, 131, 189));
		break;
		case "Normal":
			btn.setBackground(new Color(210, 210, 210));
		break;
		case "Ps�quico":
			btn.setBackground(new Color(234, 137, 154));
		break;
		case "Veneno":
			btn.setBackground(new Color(125, 33, 129));
			btn.setForeground(new Color(255, 255, 255));
		break;
		}
	}
	
	public void combatir(Pokemon p1, Pokemon p2) {
		hp_aux_aliado=p1.getHp();
		hp_aux_rival=p2.getHp();
		
		JLabel hp_restantes_aliado = new JLabel(String.valueOf(p1.getHp()));
		hp_restantes_aliado.setBounds(80, 525, 30, 20);
		contentPane.add(hp_restantes_aliado);
		
		JLabel hp_restantes_rival = new JLabel(String.valueOf(p2.getHp()));
		hp_restantes_rival.setBounds(450, 325, 30, 20);
		contentPane.add(hp_restantes_rival);
		
		JProgressBar hp_Aliado = new JProgressBar();
		hp_Aliado.setMaximum(p1.getHp());
		hp_Aliado.setValue(p1.getHp());
		hp_Aliado.setForeground(new Color(0, 255, 0));
		hp_Aliado.setBounds(30, 500, 100, 20);
		contentPane.add(hp_Aliado);
		
		JProgressBar hp_rival = new JProgressBar();
		hp_rival.setMaximum(p2.getHp());
		hp_rival.setValue(p2.getHp());
		hp_rival.setForeground(new Color(0, 255, 0));
		hp_rival.setBounds(400, 300, 100, 20);
		contentPane.add(hp_rival);
		
		JButton mov_1 = new JButton(p1.getMovimiento1().getNombreMov());
		mov_1.setBounds(130, 575, 150, 40);
		contentPane.add(mov_1);
		
		JButton mov_2 = new JButton(p1.getMovimiento2().getNombreMov());
		mov_2.setBounds(285, 575, 150, 40);
		contentPane.add(mov_2);
		
		JButton mov_3 = new JButton(p1.getMovimiento3().getNombreMov());
		mov_3.setBounds(130, 620, 150, 40);
		contentPane.add(mov_3);
		
		JButton mov_4 = new JButton(p1.getMovimiento4().getNombreMov());
		mov_4.setBounds(285, 620, 150, 40);
		contentPane.add(mov_4);
		
		JTextArea txt = new JTextArea();
		txt.setBounds(600, 150, 260, 600);
		Font fuente = new Font("Courier", Font.BOLD, 12);
		txt.setBorder(BorderFactory.createCompoundBorder(txt.getBorder(), BorderFactory.createEmptyBorder(5, 15, 5, 15)));
		txt.setFont(fuente);
		txt.setBackground(new Color(50, 50, 50));
		txt.setForeground(new Color(239,184,9));
		txt.setEditable(false);
		contentPane.add(txt);
		
		JScrollPane sp = new JScrollPane(txt);
		sp.setBounds(600, 150, 260, 600);
		sp.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		contentPane.add(sp);
		
		cambiarBG_Mov(p1.getMovimiento1(), mov_1);
		cambiarBG_Mov(p1.getMovimiento2(), mov_2);
		cambiarBG_Mov(p1.getMovimiento3(), mov_3);
		cambiarBG_Mov(p1.getMovimiento4(), mov_4);
		
		mov_1.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				int hp_Restantes_rival = hp_aux_rival-calcularDa�o(p1, p2, p1.getMovimiento1());
				restarHP_Rival(hp_Restantes_rival, p2, hp_restantes_rival, hp_rival, mov_1, mov_2, mov_3, mov_4);
				
				cont++;
				
				
				String cad1 = "TURNO "+cont+"\n\n"+p1.getNombre()+ " us� " + p1.getMovimiento1().getNombreMov()+ 
						"\n"+p2.getNombre()+" recibi� "+String.valueOf(calcularDa�o(p1, p2, p1.getMovimiento1()))+" de da�o\n";
				txt.setText(cad1);
				
				LinkedList<Integer> damages = new LinkedList<Integer>();
				damages.add(calcularDa�o(p2, p1, p2.getMovimiento1()));
				damages.add(calcularDa�o(p2, p1, p2.getMovimiento2()));
				damages.add(calcularDa�o(p2, p1, p2.getMovimiento3()));
				damages.add(calcularDa�o(p2, p1, p2.getMovimiento4()));
				damages.sort(null);
				
				
				
				
				if(damages.getLast() <= maxDmg(p2, p1, p2.getMovimiento1()) && damages.getLast() >= minDmg(p2, p1, p2.getMovimiento1())) {
					int hp_Restantes_aliado = hp_aux_aliado-damages.getLast();
					restarHP_Aliado(hp_Restantes_aliado, p1, hp_restantes_aliado, hp_Aliado, mov_1, mov_2, mov_3, mov_4);
					
					cad2="\n"+cad1+"\n"+p2.getNombre()+ " us� " + p2.getMovimiento1().getNombreMov()+ 
							"\n"+p1.getNombre()+" recibi� "+String.valueOf(calcularDa�o(p2, p1, p2.getMovimiento1()))+" de da�o\n";
					txt.setText(cad2);
				}else if(damages.getLast() <= maxDmg(p2, p1, p2.getMovimiento2()) && damages.getLast() >= minDmg(p2, p1, p2.getMovimiento2())) {
					int hp_Restantes_aliado = hp_aux_aliado-calcularDa�o(p2, p1, p2.getMovimiento2());
					restarHP_Aliado(hp_Restantes_aliado, p1, hp_restantes_aliado, hp_Aliado, mov_1, mov_2, mov_3, mov_4);
					
					cad2="\n"+cad1+"\n"+p2.getNombre()+ " us� " + p2.getMovimiento2().getNombreMov()+ 
								"\n"+p1.getNombre()+" recibi� "+String.valueOf(calcularDa�o(p2, p1, p2.getMovimiento2()))+" de da�o\n";
					txt.setText(cad2);
				}else if(damages.getLast() <= maxDmg(p2, p1, p2.getMovimiento3()) && damages.getLast() >= minDmg(p2, p1, p2.getMovimiento3())) {
					int hp_Restantes_aliado = hp_aux_aliado-calcularDa�o(p2, p1, p2.getMovimiento3());
					restarHP_Aliado(hp_Restantes_aliado, p1, hp_restantes_aliado, hp_Aliado, mov_1, mov_2, mov_3, mov_4);
					
					cad2="\n"+cad1+"\n"+p2.getNombre()+ " us� " + p2.getMovimiento3().getNombreMov()+ 
								"\n"+p1.getNombre()+" recibi� "+String.valueOf(calcularDa�o(p2, p1, p2.getMovimiento3()))+" de da�o\n";
					txt.setText(cad2);
				}else if(damages.getLast() <= maxDmg(p2, p1, p2.getMovimiento4()) && damages.getLast() >= minDmg(p2, p1, p2.getMovimiento4()) ) {
					int hp_Restantes_aliado = hp_aux_aliado-calcularDa�o(p2, p1, p2.getMovimiento4());
					restarHP_Aliado(hp_Restantes_aliado, p1, hp_restantes_aliado, hp_Aliado, mov_1, mov_2, mov_3, mov_4);
					
					cad2="\n"+cad1+"\n"+p2.getNombre()+ " us� " + p2.getMovimiento4().getNombreMov()+ 
								"\n"+p1.getNombre()+" recibi� "+String.valueOf(calcularDa�o(p2, p1, p2.getMovimiento4()))+" de da�o\n";
					txt.setText(cad2);
				}
					
				
				
				
				/*
				
				switch (random_Move) {
				case 1:
					int hp_Restantes_aliado = hp_aux_aliado-calcularDa�o(p2, p1, p2.getMovimiento1());
					restarHP_Aliado(hp_Restantes_aliado, p1, hp_restantes_aliado, hp_Aliado, mov_1, mov_2, mov_3, mov_4);
					cad2="\n"+cad1+"\n"+p2.getNombre()+ " us� " + p2.getMovimiento1().getNombreMov()+ 
							"\n"+p1.getNombre()+" recibi� "+String.valueOf(calcularDa�o(p2, p1, p2.getMovimiento1()))+" de da�o\n";
					txt.setText(cad2);
					
				break;
				case 2:
					 hp_Restantes_aliado = hp_aux_aliado-calcularDa�o(p2, p1, p2.getMovimiento2());
					 restarHP_Aliado(hp_Restantes_aliado, p1, hp_restantes_aliado, hp_Aliado, mov_1, mov_2, mov_3, mov_4);
					 cad2="\n"+cad1+"\n"+p2.getNombre()+ " us� " + p2.getMovimiento2().getNombreMov()+ 
								"\n"+p1.getNombre()+" recibi� "+String.valueOf(calcularDa�o(p2, p1, p2.getMovimiento2()))+" de da�o\n";
					 txt.setText(cad2);
				break;
				case 3:
					 int hp_Restantes_aliado = hp_aux_aliado-calcularDa�o(p2, p1, p2.getMovimiento3());
					 restarHP_Aliado(hp_Restantes_aliado, p1, hp_restantes_aliado, hp_Aliado, mov_1, mov_2, mov_3, mov_4);
					 cad2="\n"+cad1+"\n"+p2.getNombre()+ " us� " + p2.getMovimiento3().getNombreMov()+ 
								"\n"+p1.getNombre()+" recibi� "+String.valueOf(calcularDa�o(p2, p1, p2.getMovimiento3()))+" de da�o\n";
					 txt.setText(cad2);
				break;
				case 4:
					int hp_Restantes_aliado = hp_aux_aliado-calcularDa�o(p2, p1, p2.getMovimiento4());
					restarHP_Aliado(hp_Restantes_aliado, p1, hp_restantes_aliado, hp_Aliado, mov_1, mov_2, mov_3, mov_4);
					cad2="\n"+cad1+"\n"+p2.getNombre()+ " us� " + p2.getMovimiento4().getNombreMov()+ 
							"\n"+p1.getNombre()+" recibi� "+String.valueOf(calcularDa�o(p2, p1, p2.getMovimiento4()))+" de da�o\n";
					txt.setText(cad2);
				break;
				}*/
				txt.setText(aux+cad2);
				aux=txt.getText();
			}
		});
		
		mov_2.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
		mov_3.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		});
	}
	
	public int calcularDa�o(Pokemon p1, Pokemon p2, Movimientos m) {
		if(p1.getTipo1().getNombreTipo().equalsIgnoreCase(m.getTipoMov().getNombreTipo())) {
			stab = 1.5;
		}else if (p1.getTipo2()!=null && p1.getTipo2().getNombreTipo().equalsIgnoreCase(m.getTipoMov().getNombreTipo())) {
			stab = 1.5;
		}else 
			stab = 1.0;
		
		switch(m.getTipoMov().getNombreTipo()) {
		case "Planta":
			if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("agua") && p2.getTipo2()==null) {
				efectividad=2.0;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("veneno") && p2.getTipo2()==null) {
				efectividad=0.5;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("normal") && p2.getTipo2()==null) {
				efectividad=1.0;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("ps�quico") && p2.getTipo2()==null) {
				efectividad=1.0;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("planta") && p2.getTipo2()==null) {
				efectividad=0.5;
			}else if ((p2.getTipo1().getNombreTipo().equalsIgnoreCase("agua") && p2.getTipo2().getNombreTipo().equalsIgnoreCase("ps�quico")) || (p2.getTipo1().getNombreTipo().equalsIgnoreCase("ps�quico") && p2.getTipo2().getNombreTipo().equalsIgnoreCase("agua"))) {
				efectividad=2.0;
			}else if ((p2.getTipo1().getNombreTipo().equalsIgnoreCase("planta") && p2.getTipo2().getNombreTipo().equalsIgnoreCase("veneno")) || (p2.getTipo1().getNombreTipo().equalsIgnoreCase("veneno") && p2.getTipo2().getNombreTipo().equalsIgnoreCase("planta"))) {
				efectividad=0.25;
			}
		break;
		case "Agua":
			if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("agua") && p2.getTipo2()==null) {
				efectividad=0.5;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("veneno") && p2.getTipo2()==null) {
				efectividad=1.0;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("normal") && p2.getTipo2()==null) {
				efectividad=1.0;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("ps�quico") && p2.getTipo2()==null) {
				efectividad=1.0;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("planta") && p2.getTipo2()==null) {
				efectividad=0.5;
			}else if ((p2.getTipo1().getNombreTipo().equalsIgnoreCase("agua") && p2.getTipo2().getNombreTipo().equalsIgnoreCase("ps�quico")) || (p2.getTipo1().getNombreTipo().equalsIgnoreCase("ps�quico") && p2.getTipo2().getNombreTipo().equalsIgnoreCase("agua"))) {
				efectividad=0.5;
			}else if ((p2.getTipo1().getNombreTipo().equalsIgnoreCase("planta") && p2.getTipo2().getNombreTipo().equalsIgnoreCase("veneno")) || (p2.getTipo1().getNombreTipo().equalsIgnoreCase("veneno") && p2.getTipo2().getNombreTipo().equalsIgnoreCase("planta"))) {
				efectividad=0.5;
			}
		break;
		case "Normal":
			if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("agua") && p2.getTipo2()==null) {
				efectividad=1.0;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("veneno") && p2.getTipo2()==null) {
				efectividad=1.0;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("normal") && p2.getTipo2()==null) {
				efectividad=1.0;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("ps�quico") && p2.getTipo2()==null) {
				efectividad=1.0;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("planta") && p2.getTipo2()==null) {
				efectividad=1.0;
			}else if ((p2.getTipo1().getNombreTipo().equalsIgnoreCase("agua") && p2.getTipo1().getNombreTipo().equalsIgnoreCase("ps�quico")) || (p2.getTipo1().getNombreTipo().equalsIgnoreCase("ps�quico") && p2.getTipo2().getNombreTipo().equalsIgnoreCase("agua"))) {
				efectividad=1.0;
			}else if ((p2.getTipo1().getNombreTipo().equalsIgnoreCase("planta") && p2.getTipo2().getNombreTipo().equalsIgnoreCase("veneno")) || (p2.getTipo1().getNombreTipo().equalsIgnoreCase("veneno") && p2.getTipo2().getNombreTipo().equalsIgnoreCase("planta"))) {
				efectividad=1.0;
			}
		break;
		case "Ps�quico":
			if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("agua") && p2.getTipo2()==null) {
				efectividad=1.0;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("veneno") && p2.getTipo2()==null) {
				efectividad=2.0;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("normal") && p2.getTipo2()==null) {
				efectividad=1.0;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("ps�quico") && p2.getTipo2()==null) {
				efectividad=0.5;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("planta") && p2.getTipo2()==null) {
				efectividad=1.0;
			}else if ((p2.getTipo1().getNombreTipo().equalsIgnoreCase("agua") && p2.getTipo2().getNombreTipo().equalsIgnoreCase("ps�quico")) || (p2.getTipo1().getNombreTipo().equalsIgnoreCase("ps�quico") && p2.getTipo2().getNombreTipo().equalsIgnoreCase("agua"))) {
				efectividad=0.5;
			}else if ((p2.getTipo1().getNombreTipo().equalsIgnoreCase("planta") && p2.getTipo2().getNombreTipo().equalsIgnoreCase("veneno")) || (p2.getTipo1().getNombreTipo().equalsIgnoreCase("veneno") && p2.getTipo2().getNombreTipo().equalsIgnoreCase("planta"))) {
				efectividad=2.0;
			}
		break;
		case "Veneno":
			if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("agua") && p2.getTipo2()==null) {
				efectividad=1.0;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("veneno") && p2.getTipo2()==null) {
				efectividad=0.5;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("normal") && p2.getTipo2()==null) {
				efectividad=1.0;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("ps�quico") && p2.getTipo2()==null) {
				efectividad=1.0;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("planta") && p2.getTipo2()==null) {
				efectividad=2.0;
			}else if ((p2.getTipo1().getNombreTipo().equalsIgnoreCase("agua") && p2.getTipo2().getNombreTipo().equalsIgnoreCase("ps�quico")) || (p2.getTipo1().getNombreTipo().equalsIgnoreCase("ps�quico") && p2.getTipo2().getNombreTipo().equalsIgnoreCase("agua"))) {
				efectividad=1.0;
			}else if ((p2.getTipo1().getNombreTipo().equalsIgnoreCase("planta") && p2.getTipo2().getNombreTipo().equalsIgnoreCase("veneno")) || (p2.getTipo1().getNombreTipo().equalsIgnoreCase("veneno") && p2.getTipo2().getNombreTipo().equalsIgnoreCase("planta"))) {
				efectividad=1.0;
			}
		break;		
		}		
		
		double atk_def = ((0.2 * p1.getNivel() +1) * p1.getAtk() * m.getPotencia() + 2) / (25 * p2.getDef());
		double dmg = 0.01 * stab * efectividad * randomNumberGenerator(85, 100) * atk_def;
		
		dmgg = (int) dmg;
		return dmgg;				
	}
	
	public int minDmg(Pokemon p1, Pokemon p2, Movimientos m) {
		if(p1.getTipo1().getNombreTipo().equalsIgnoreCase(m.getTipoMov().getNombreTipo())) {
			stab = 1.5;
		}else if (p1.getTipo2()!=null && p1.getTipo2().getNombreTipo().equalsIgnoreCase(m.getTipoMov().getNombreTipo())) {
			stab = 1.5;
		}else 
			stab = 1.0;
		
		switch(m.getTipoMov().getNombreTipo()) {
		case "Planta":
			if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("agua") && p2.getTipo2()==null) {
				efectividad=2.0;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("veneno") && p2.getTipo2()==null) {
				efectividad=0.5;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("normal") && p2.getTipo2()==null) {
				efectividad=1.0;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("ps�quico") && p2.getTipo2()==null) {
				efectividad=1.0;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("planta") && p2.getTipo2()==null) {
				efectividad=0.5;
			}else if ((p2.getTipo1().getNombreTipo().equalsIgnoreCase("agua") && p2.getTipo2().getNombreTipo().equalsIgnoreCase("ps�quico")) || (p2.getTipo1().getNombreTipo().equalsIgnoreCase("ps�quico") && p2.getTipo2().getNombreTipo().equalsIgnoreCase("agua"))) {
				efectividad=2.0;
			}else if ((p2.getTipo1().getNombreTipo().equalsIgnoreCase("planta") && p2.getTipo2().getNombreTipo().equalsIgnoreCase("veneno")) || (p2.getTipo1().getNombreTipo().equalsIgnoreCase("veneno") && p2.getTipo2().getNombreTipo().equalsIgnoreCase("planta"))) {
				efectividad=0.25;
			}
		break;
		case "Agua":
			if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("agua") && p2.getTipo2()==null) {
				efectividad=0.5;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("veneno") && p2.getTipo2()==null) {
				efectividad=1.0;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("normal") && p2.getTipo2()==null) {
				efectividad=1.0;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("ps�quico") && p2.getTipo2()==null) {
				efectividad=1.0;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("planta") && p2.getTipo2()==null) {
				efectividad=0.5;
			}else if ((p2.getTipo1().getNombreTipo().equalsIgnoreCase("agua") && p2.getTipo2().getNombreTipo().equalsIgnoreCase("ps�quico")) || (p2.getTipo1().getNombreTipo().equalsIgnoreCase("ps�quico") && p2.getTipo2().getNombreTipo().equalsIgnoreCase("agua"))) {
				efectividad=0.5;
			}else if ((p2.getTipo1().getNombreTipo().equalsIgnoreCase("planta") && p2.getTipo2().getNombreTipo().equalsIgnoreCase("veneno")) || (p2.getTipo1().getNombreTipo().equalsIgnoreCase("veneno") && p2.getTipo2().getNombreTipo().equalsIgnoreCase("planta"))) {
				efectividad=0.5;
			}
		break;
		case "Normal":
			if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("agua") && p2.getTipo2()==null) {
				efectividad=1.0;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("veneno") && p2.getTipo2()==null) {
				efectividad=1.0;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("normal") && p2.getTipo2()==null) {
				efectividad=1.0;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("ps�quico") && p2.getTipo2()==null) {
				efectividad=1.0;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("planta") && p2.getTipo2()==null) {
				efectividad=1.0;
			}else if ((p2.getTipo1().getNombreTipo().equalsIgnoreCase("agua") && p2.getTipo1().getNombreTipo().equalsIgnoreCase("ps�quico")) || (p2.getTipo1().getNombreTipo().equalsIgnoreCase("ps�quico") && p2.getTipo2().getNombreTipo().equalsIgnoreCase("agua"))) {
				efectividad=1.0;
			}else if ((p2.getTipo1().getNombreTipo().equalsIgnoreCase("planta") && p2.getTipo2().getNombreTipo().equalsIgnoreCase("veneno")) || (p2.getTipo1().getNombreTipo().equalsIgnoreCase("veneno") && p2.getTipo2().getNombreTipo().equalsIgnoreCase("planta"))) {
				efectividad=1.0;
			}
		break;
		case "Ps�quico":
			if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("agua") && p2.getTipo2()==null) {
				efectividad=1.0;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("veneno") && p2.getTipo2()==null) {
				efectividad=2.0;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("normal") && p2.getTipo2()==null) {
				efectividad=1.0;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("ps�quico") && p2.getTipo2()==null) {
				efectividad=0.5;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("planta") && p2.getTipo2()==null) {
				efectividad=1.0;
			}else if ((p2.getTipo1().getNombreTipo().equalsIgnoreCase("agua") && p2.getTipo2().getNombreTipo().equalsIgnoreCase("ps�quico")) || (p2.getTipo1().getNombreTipo().equalsIgnoreCase("ps�quico") && p2.getTipo2().getNombreTipo().equalsIgnoreCase("agua"))) {
				efectividad=0.5;
			}else if ((p2.getTipo1().getNombreTipo().equalsIgnoreCase("planta") && p2.getTipo2().getNombreTipo().equalsIgnoreCase("veneno")) || (p2.getTipo1().getNombreTipo().equalsIgnoreCase("veneno") && p2.getTipo2().getNombreTipo().equalsIgnoreCase("planta"))) {
				efectividad=2.0;
			}
		break;
		case "Veneno":
			if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("agua") && p2.getTipo2()==null) {
				efectividad=1.0;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("veneno") && p2.getTipo2()==null) {
				efectividad=0.5;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("normal") && p2.getTipo2()==null) {
				efectividad=1.0;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("ps�quico") && p2.getTipo2()==null) {
				efectividad=1.0;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("planta") && p2.getTipo2()==null) {
				efectividad=2.0;
			}else if ((p2.getTipo1().getNombreTipo().equalsIgnoreCase("agua") && p2.getTipo2().getNombreTipo().equalsIgnoreCase("ps�quico")) || (p2.getTipo1().getNombreTipo().equalsIgnoreCase("ps�quico") && p2.getTipo2().getNombreTipo().equalsIgnoreCase("agua"))) {
				efectividad=1.0;
			}else if ((p2.getTipo1().getNombreTipo().equalsIgnoreCase("planta") && p2.getTipo2().getNombreTipo().equalsIgnoreCase("veneno")) || (p2.getTipo1().getNombreTipo().equalsIgnoreCase("veneno") && p2.getTipo2().getNombreTipo().equalsIgnoreCase("planta"))) {
				efectividad=1.0;
			}
		break;		
		}		
		
		double atk_def = ((0.2 * p1.getNivel() +1) * p1.getAtk() * m.getPotencia() + 2) / (25 * p2.getDef());
		double dmg = 0.01 * stab * efectividad * 85 * atk_def;
		
		dmgg = (int) dmg;
		return dmgg;				
	}
	
	public int maxDmg(Pokemon p1, Pokemon p2, Movimientos m) {
		if(p1.getTipo1().getNombreTipo().equalsIgnoreCase(m.getTipoMov().getNombreTipo())) {
			stab = 1.5;
		}else if (p1.getTipo2()!=null && p1.getTipo2().getNombreTipo().equalsIgnoreCase(m.getTipoMov().getNombreTipo())) {
			stab = 1.5;
		}else 
			stab = 1.0;
		
		switch(m.getTipoMov().getNombreTipo()) {
		case "Planta":
			if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("agua") && p2.getTipo2()==null) {
				efectividad=2.0;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("veneno") && p2.getTipo2()==null) {
				efectividad=0.5;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("normal") && p2.getTipo2()==null) {
				efectividad=1.0;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("ps�quico") && p2.getTipo2()==null) {
				efectividad=1.0;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("planta") && p2.getTipo2()==null) {
				efectividad=0.5;
			}else if ((p2.getTipo1().getNombreTipo().equalsIgnoreCase("agua") && p2.getTipo2().getNombreTipo().equalsIgnoreCase("ps�quico")) || (p2.getTipo1().getNombreTipo().equalsIgnoreCase("ps�quico") && p2.getTipo2().getNombreTipo().equalsIgnoreCase("agua"))) {
				efectividad=2.0;
			}else if ((p2.getTipo1().getNombreTipo().equalsIgnoreCase("planta") && p2.getTipo2().getNombreTipo().equalsIgnoreCase("veneno")) || (p2.getTipo1().getNombreTipo().equalsIgnoreCase("veneno") && p2.getTipo2().getNombreTipo().equalsIgnoreCase("planta"))) {
				efectividad=0.25;
			}
		break;
		case "Agua":
			if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("agua") && p2.getTipo2()==null) {
				efectividad=0.5;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("veneno") && p2.getTipo2()==null) {
				efectividad=1.0;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("normal") && p2.getTipo2()==null) {
				efectividad=1.0;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("ps�quico") && p2.getTipo2()==null) {
				efectividad=1.0;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("planta") && p2.getTipo2()==null) {
				efectividad=0.5;
			}else if ((p2.getTipo1().getNombreTipo().equalsIgnoreCase("agua") && p2.getTipo2().getNombreTipo().equalsIgnoreCase("ps�quico")) || (p2.getTipo1().getNombreTipo().equalsIgnoreCase("ps�quico") && p2.getTipo2().getNombreTipo().equalsIgnoreCase("agua"))) {
				efectividad=0.5;
			}else if ((p2.getTipo1().getNombreTipo().equalsIgnoreCase("planta") && p2.getTipo2().getNombreTipo().equalsIgnoreCase("veneno")) || (p2.getTipo1().getNombreTipo().equalsIgnoreCase("veneno") && p2.getTipo2().getNombreTipo().equalsIgnoreCase("planta"))) {
				efectividad=0.5;
			}
		break;
		case "Normal":
			if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("agua") && p2.getTipo2()==null) {
				efectividad=1.0;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("veneno") && p2.getTipo2()==null) {
				efectividad=1.0;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("normal") && p2.getTipo2()==null) {
				efectividad=1.0;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("ps�quico") && p2.getTipo2()==null) {
				efectividad=1.0;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("planta") && p2.getTipo2()==null) {
				efectividad=1.0;
			}else if ((p2.getTipo1().getNombreTipo().equalsIgnoreCase("agua") && p2.getTipo1().getNombreTipo().equalsIgnoreCase("ps�quico")) || (p2.getTipo1().getNombreTipo().equalsIgnoreCase("ps�quico") && p2.getTipo2().getNombreTipo().equalsIgnoreCase("agua"))) {
				efectividad=1.0;
			}else if ((p2.getTipo1().getNombreTipo().equalsIgnoreCase("planta") && p2.getTipo2().getNombreTipo().equalsIgnoreCase("veneno")) || (p2.getTipo1().getNombreTipo().equalsIgnoreCase("veneno") && p2.getTipo2().getNombreTipo().equalsIgnoreCase("planta"))) {
				efectividad=1.0;
			}
		break;
		case "Ps�quico":
			if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("agua") && p2.getTipo2()==null) {
				efectividad=1.0;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("veneno") && p2.getTipo2()==null) {
				efectividad=2.0;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("normal") && p2.getTipo2()==null) {
				efectividad=1.0;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("ps�quico") && p2.getTipo2()==null) {
				efectividad=0.5;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("planta") && p2.getTipo2()==null) {
				efectividad=1.0;
			}else if ((p2.getTipo1().getNombreTipo().equalsIgnoreCase("agua") && p2.getTipo2().getNombreTipo().equalsIgnoreCase("ps�quico")) || (p2.getTipo1().getNombreTipo().equalsIgnoreCase("ps�quico") && p2.getTipo2().getNombreTipo().equalsIgnoreCase("agua"))) {
				efectividad=0.5;
			}else if ((p2.getTipo1().getNombreTipo().equalsIgnoreCase("planta") && p2.getTipo2().getNombreTipo().equalsIgnoreCase("veneno")) || (p2.getTipo1().getNombreTipo().equalsIgnoreCase("veneno") && p2.getTipo2().getNombreTipo().equalsIgnoreCase("planta"))) {
				efectividad=2.0;
			}
		break;
		case "Veneno":
			if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("agua") && p2.getTipo2()==null) {
				efectividad=1.0;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("veneno") && p2.getTipo2()==null) {
				efectividad=0.5;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("normal") && p2.getTipo2()==null) {
				efectividad=1.0;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("ps�quico") && p2.getTipo2()==null) {
				efectividad=1.0;
			}else if (p2.getTipo1().getNombreTipo().equalsIgnoreCase("planta") && p2.getTipo2()==null) {
				efectividad=2.0;
			}else if ((p2.getTipo1().getNombreTipo().equalsIgnoreCase("agua") && p2.getTipo2().getNombreTipo().equalsIgnoreCase("ps�quico")) || (p2.getTipo1().getNombreTipo().equalsIgnoreCase("ps�quico") && p2.getTipo2().getNombreTipo().equalsIgnoreCase("agua"))) {
				efectividad=1.0;
			}else if ((p2.getTipo1().getNombreTipo().equalsIgnoreCase("planta") && p2.getTipo2().getNombreTipo().equalsIgnoreCase("veneno")) || (p2.getTipo1().getNombreTipo().equalsIgnoreCase("veneno") && p2.getTipo2().getNombreTipo().equalsIgnoreCase("planta"))) {
				efectividad=1.0;
			}
		break;		
		}		
		
		double atk_def = ((0.2 * p1.getNivel() +1) * p1.getAtk() * m.getPotencia() + 2) / (25 * p2.getDef());
		double dmg = 0.01 * stab * efectividad * 100 * atk_def;
		
		dmgg = (int) dmg;
		return dmgg;				
	}
	
	public void restarHP_Rival(int n, Pokemon pkmn, JLabel lb, JProgressBar pb, JButton btn1, JButton btn2, JButton btn3, JButton btn4) {
		lb.setText(String.valueOf(n));
		pb.setValue(n);
		hp_aux_rival=n;
				
		if (n<=pkmn.getHp()/2) {
			pb.setForeground(new Color(229, 190, 1));
		}if (n<=pkmn.getHp()/8) {
			pb.setForeground(new Color(203, 0, 0));
		}if (n <=0) {
			lb.setText(String.valueOf(0));
			btn1.setEnabled(false);
			btn2.setEnabled(false);
			btn3.setEnabled(false);
			btn4.setEnabled(false);
		}
	}
	
	public void restarHP_Aliado(int n, Pokemon pkmn, JLabel lb, JProgressBar pb, JButton btn1, JButton btn2, JButton btn3, JButton btn4) {
		lb.setText(String.valueOf(n));
		pb.setValue(n);
		hp_aux_aliado=n;
				
		if (n<=pkmn.getHp()/2) {
			pb.setForeground(new Color(229, 190, 1));
		}if (n<=pkmn.getHp()/8) {
			pb.setForeground(new Color(203, 0, 0));
		}if (n <=0) {
			lb.setText(String.valueOf(0));
			btn1.setEnabled(false);
			btn2.setEnabled(false);
			btn3.setEnabled(false);
			btn4.setEnabled(false);
		}
	}
	
	public int randomNumberGenerator(int min, int max) {
		return (int) ((Math.random() * (max-min))+min);
	}
	
	
}