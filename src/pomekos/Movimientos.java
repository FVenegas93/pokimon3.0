package pomekos;



public class Movimientos {
	protected String nombreMov;
	protected int potencia;
	protected Tipo tipoMov;
	protected int pp_max;
	protected double stab;
	protected double efectividad;
	
	
	public Movimientos(String nombreMov, int potencia, Tipo tipoMov, int pp_max) {
		this.nombreMov = nombreMov;
		this.potencia = potencia;
		this.tipoMov = tipoMov;
		this.pp_max = pp_max;
		
	}


	public String getNombreMov() {
		return nombreMov;
	}

	public void setNombreMov(String nombreMov) {
		this.nombreMov = nombreMov;
	}

	public int getPotencia() {
		return potencia;
	}

	public void setPotencia(int potencia) {
		this.potencia = potencia;
	}

	public Tipo getTipoMov() {
		return tipoMov;
	}

	public void setTipoMov(Tipo tipoMov) {
		this.tipoMov = tipoMov;
	}
	
	public int getPp_max() {
		return pp_max;
	}

	public void setPp_max(int pp_max) {
		this.pp_max = pp_max;
	}



}
